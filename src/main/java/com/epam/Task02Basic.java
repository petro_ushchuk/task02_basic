package com.epam;

import java.util.Scanner;

public class Task02Basic {
    public static void main(String[] args) {
        int start;
        int end;
        int n;
        int f1;
        int f2;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the interval: ");
        start = scanner.nextInt();
        end = scanner.nextInt();
        OddAndEven firstProg = new OddAndEven(start, end);
        firstProg.showOddFromStart();
        firstProg.showEvenFromEnd();
        firstProg.showSumOdd();
        firstProg.showSumEven();

        System.out.println("Enter the size of set: ");
        n = scanner.nextInt();
        Fibonacci secProg = new Fibonacci(n);
        f1 = secProg.getOddMax();
        f2 = secProg.getEvenMax();
        System.out.println(f1 + " " + f2);
        secProg.showPercentOfOddAndEven();
    }
}
