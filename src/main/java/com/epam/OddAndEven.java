package com.epam;

/**
 * Class OddAndEven prints odd numbers from start to the end of
 * interval and even from end to start. Also prints sum of odd
 * and even numbers.
 * <p>
 * @author Petro Ushchuk
 * @version 1.1
 */
public class OddAndEven {
    /**
     * The begining of the interval
     */
    private int start;
    /**
     * The end of the interval
     */
    private int end;
    /**
     * Sum of odd
     */
    private int sumOdd;
    /**
     * Sum of even
     */
    private int sumEven;

    /**
     * Constructs a new OddAndEven by setting the start and end of interval
     * @param start - start of interval
     * @param end   - end of interval
     */
    public OddAndEven(final int start, final int end) {
        this.start = start;
        this.end = end;
        for (int i = start + (1 - start % 2); i <= end; i += 2) {
            sumOdd += i;
        }
        for (int i = end - end % 2; i >= start; i -= 2) {
            sumEven += i;
        }
    }

    /**
     * Returns value of the end of interval
     * @return value of the end of interval
     */
    public int getEnd() {
        return end;
    }

    /**
     * Returns value of the start of interval
     * @return value of the start of interval
     */
    public int getStart() {
        return start;
    }

    /**
     * Returns sum of odd numbers
     * @return sum of odd numbers
     */
    public int getSumOdd() {
        return sumOdd;
    }

    /**
     * Returns sum of odd numbers
     * @return sum of odd numbers
     */
    public int getSumEven() {
        return sumEven;
    }

    /**
     * Sets the end of interval
     * @param end sets the end of interval
     */
    public void setEnd(final int end) {
        this.end = end;
    }

    /**
     * Sets the start of interval
     * @param start sets the start of interval
     */
    public void setStart(final int start) {
        this.start = start;
    }

    /**
     * Prints odd numbers from start to the end
     */
    public void showOddFromStart() {
        for (int i = start + (1 - start % 2); i <= end; i += 2) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    /**
     * Prints even numbers from end to start
     */
    public void showEvenFromEnd() {
        for (int i = end - end % 2; i >= start; i -= 2) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    /**
     * Prints sum of even numbers
     */
    public void showSumEven() {
        System.out.println(sumEven);
    }

    /**
     * Prints sum of even numbers
     */
    public void showSumOdd() {
        System.out.println(sumOdd);
    }
}
