package com.epam;

/**
 * Class Fibonacci build Fibonacci numbers, finding the biggest even
 * number and the biggest odd number. Prints percentage of odd and
 * even Fibonacci numbers.
 * <p>
 * @author Petro Ushchuk
 * @version 1.1
 */
public class Fibonacci {
    /**
     * size of set
     */
    private int sizeOfSet;
    /**
     * fibbonacci numbers array
     */
    private int[] fibo;
    /**
     * number of odd on the interval
     */
    private int numOfOdd;
    /**
     * number of even on the interval
     */
    private int numOfEven;

    /**
     * Constructs a new Fibbonacci by setting the size
     *
     * @param sizeOfSet - size of set
     */
    public Fibonacci(final int sizeOfSet) {
        this.sizeOfSet = sizeOfSet;
        fibo = new int[sizeOfSet];
        fibo[0] = 1;
        fibo[1] = 1;
        for (int i = 2; i < sizeOfSet; i++) {
            fibo[i] = fibo[i - 1] + fibo[i - 2];
        }
    }

    /**
     * Sets the size
     * @param sizeOfSet sets the size
     */
    public void setsizeOfSet(final int sizeOfSet) {
        this.sizeOfSet = sizeOfSet;
    }

    /**
     * Returns value of even of the set
     * @return value of even of the set
     */
    public int getNumOfEven() {
        return numOfEven;
    }

    /**
     * Returns value of odd of the set
     * @return value of odd of the set
     */
    public int getNumOfOdd() {
        return numOfOdd;
    }

    /**
     * Returns value of size of the set
     * @return value of size of the set
     */
    public int getsizeOfSet() {
        return sizeOfSet;
    }

    /**
     * Returns f1 - biggest odd number
     * @return f1 - biggest odd number
     */
    public int getOddMax() {
        for (int i = sizeOfSet - 1; i >= 0; i--) {
            if (fibo[i] % 2 != 0) {
                return fibo[i];
            }
        }
        return -1;
    }

    /**
     * Returns f2 - biggest even number
     * @return f2 - biggest even number
     */
    public int getEvenMax() {
        for (int i = sizeOfSet - 1; i >= 0; i--) {
            if (fibo[i] % 2 == 0) {
                return fibo[i];
            }
        }
        return -1;
    }

    /**
     * Prints percentage of odd and even Fibonacci numbers
     */
    public void showPercentOfOddAndEven() {
        for (int i = 0; i < sizeOfSet; i++) {
            if (fibo[i] % 2 == 0) {
                numOfEven++;
            } else {
                numOfOdd++;
            }
        }
        System.out.println(((double) numOfOdd / sizeOfSet) + ((double) numOfEven / sizeOfSet));
    }
}
